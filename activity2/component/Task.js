
import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { CheckBox, TouchableOpacity } from "react-native-web";

const Task = (props) => {
    return (
        <View style={styles.item}>
            <View style={styles.itemLeft}>
                <View style={styles.circle}></View>
                <Text style={styles.itemText}>{props.text}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    item: {
        backgroundColor: '#746AB0',
        padding: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10,
        paddingVertical: 50,
    },
    itemLeft: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    circle: {
        width: 19,
        height: 19,
        backgroundColor: '#E389B9',
        borderWidth: 3,
         borderColor: '#05ffb0',
        borderRadius: 60,
        marginRight: 15,
    },
    itemText: {
        maxWidth: '85%',
        color: '#383838',
        fontWeight: 'bold',
        fontSize: 18,
    },
});
export default Task;
